package cryptsetup

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path"
	"runtime"
	"strings"
	"syscall"

	"github.com/Sirupsen/logrus"
	"github.com/freddierice/go-losetup"
)

const (
	constCryptsetupBin      = "/sbin/cryptsetup"
	constDevMapperPrefix    = "/dev/mapper"
	constMinImageSize       = 10 * 1024 * 1024 // 10 MB
	constCryptsetupOverhead = 2 * 1024 * 1024  // 2 MB
	constFsOverhead         = 5                // (in %) 5%

	constLuksCmdFormat = "luks-format"
	constLuksCmdOpen   = "luks-open"
	constLuksCmdClose  = "luks-close"
	constLuksCmdRemove = "luks-remove"

	constVerityCmdFormat = "verity-format"
	constVerityCmdCreate = "verity-create"
	constVerityCmdRemove = "verity-remove"
	constVerityCmdVerify = "verity-verify"
)

<<<<<<< HEAD
type RawImage struct {
	ImagePath	string
	LoDev		losetup.Device
}

type Device struct {
	DevicePath	string
	FsType		string
	Mnt			string
=======
type PlainDevice struct {
	DevicePath string
	FsType     string
	Mnt        string
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
}

type VirtualDevice struct {
	Image		RawImage
	Dev			Device
}

type CryptDevice struct {
<<<<<<< HEAD
	VDev		VirtualDevice
	Name		string
	Cipher		string
	HashType	string
	KeySize		string
	Key			string
}

type VerityDevice struct {
	VDev		VirtualDevice
	HashImage	string
	Name		string
	RootHash	string
}

type CryptVerityDevice struct {
	VDev		VirtualDevice
	Name		string
	Cipher		string
	HashType	string
	KeySize		string
	Key			string
	HashImage	string
	RootHash	string
=======
	Device    PlainDevice
	BaseImage string
	Name      string
	Cipher    string
	HashType  string
	KeySize   string
	Key       string
}

type VerityDevice struct {
	Device     PlainDevice
	BaseImage  string
	Name       string
	RootHash   string
	HashDevice string
}

type CryptVerityDevice struct {
	Crypt      CryptDevice
	Verity     VerityDevice
	BaseImage  string
	HashDevice string
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
}

type DeviceAPI interface {
	Create(size int64) error
	ImportData(dataPath string) error
	Get() error
	Put() error
	Remove() error
}

// **************************** helper functions ****************************************
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

func runCmd(cmd string) (string, error) {
	runtime.LockOSThread()
	defer runtime.UnlockOSThread()
	logrus.Debugf("runCmd => cmd: %s", cmd)
	out, err := exec.Command("/bin/bash", "-c", cmd).Output()
	return string(out), err
}

func mount(source, target, fsType, options string) error {
	if rt, _ := exists(source); rt {

		flags := syscall.MS_REC

		if err := syscall.Mount(source, target, fsType, uintptr(flags), options); err != nil {
			logrus.Errorf("failed to mount source: %s  at %s, error: %s", source, target, err.Error())
			return err
		}

		return nil
	}

	return errors.New(fmt.Sprintf("source path %s does not exists", source))
}

func unmount(target string) error {
	if rt, _ := exists(target); rt {

		if err := syscall.Unmount(target, 0); err != nil {
			logrus.Errorf("failed to unmount %s, error: %s", target, err.Error())
			return err
		}

		return nil
	}

	return errors.New(fmt.Sprintf("target path %s does not exists", target))
}

func createImageFile(filePath string, size int64) error {
	// create image file if does not exists
	if rt, _ := exists(filePath); !rt {
		if _, err := os.Create(filePath); err != nil {
			logrus.Errorf("failed to create image file %s", filePath)
			return err
		}
	}

	if err := os.Truncate(filePath, size); err != nil {
		logrus.Errorf("faild to create image file %s", filePath)
		return err
	}
	return nil
}

func fsFormat(filePath, fsType, Options string) error {
	cmd := fmt.Sprintf("mkfs.%s %s", fsType, filePath)
	if out, err := runCmd(cmd); err != nil {
		logrus.Errorf("failed to format device %s, error: %s, out: %s", filePath, err.Error(), out)
		return err
	}
	return nil
}

func computeCryptOverhead(size int64) int64 {
	return int64(constCryptsetupOverhead)
}

func computeFsOverhead(size int64) int64 {
	return int64(size * constFsOverhead / 100)
}

func safeSize(size int64) int64 {
	// make sure that minimum required size
	if size < constMinImageSize {
		return int64(constMinImageSize)
	}

	return size
}

// **************************************************************************************

// *************** raw image management **************************
func (i RawImage) Create(size int64) error {
	sz := safeSize(size)
	return createImageFile(i.ImagePath, sz)
}

func (i RawImage) Get() error {
	if rt, _ := exists(i.ImagePath); ! rt {
		return errors.New(fmt.Sprintf("Image file %s does not exists", i.ImagePath))
	}
	
	// attach raw image file to loop device
	i.LoDev, err := losetup.Attach(i.ImagePath, 0, false)
	if err != nil {return err}
	
	return nil
}

func (i RawImage) Put() error {
	if err := i.LoDev.Detach(); err != nil {return err}
	return nil
}
<<<<<<< HEAD

func (i RawImage) Remove() error {
	// ignore errors if Put is already called
	_ = i.Put()
	return os.Remove(i.ImagePath)
}

func (i RawImage) devPath() string {
	return i.LoDev.Path()
=======
func (d PlainDevice) Get() error {
	return mount(d.DevicePath, d.Mnt, d.FsType, "")
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
}

// ************** plain device management ***************************
func (d Device) Create(size int64) error {
	// nothing to do for plain device
	return nil
}

func (d Device) ImportData(dataPath string) error {
	if err := fsFormat(d.DevicePath, d.FsType, ""); err != nil {
		return err
	}
	
	if err := d.Get(); err != nil { return err }
	
	defer func(){
		_ = d.Put()
	}()
	
	// copy data to the device
	cmd := fmt.Sprintf("rsync -a %s/ %s", dataPath, d.Mnt)
	
	return nil
}

func (d Device) Get() error {
	return mount(d.DevicePath, d.Mnt, d.FsType, "")
}

func (d Device) Put() error {
	return unmount(d.Mnt)
}

func (d Device) Remove() error {
	// nothing to do for plain device
	return nil
}

// *************** virtual device APIs *********************
func (d VirtualDevice) Create(size int64) error {
	sz := safeSize(size + computeFsOverhead(size))
	return d.Image.Create(sz)
}

func (d VirtualDevice) ImportData(dataPath string) error {
	if err := d.Image.Get(); err != nil {return err}
	
	// detach loop device
	defer func(){
		if err := d.Image.Put(); err != nil { return err}
	}()
	
	// perform file system format on loop mounted image file
	d.Dev.DevicePath = d.Image.devPath()
	if err := d.Dev.ImportData(dataPath); err != nil {return err}
	
	return nil
}

func (d VirtualDevice) Get() error {
	if err := d.Image.Get(); err != nil {return err}
	
	// mount virtual device
	d.Dev.DevicePath = d.Image.devPath()
	if err := d.Dev.Get(); err != nil {return err}
	
	return nil
}

func (d VirtualDevice) Put() error {
	// unmount the device
	if err := d.Dev.Put(); err != nil {return err}
	
	return d.Image.Put()
}

func (d VirtualDevice) Remove() error {
	_ = d.Put()
	return d.Image.Remove()
}

func (d VirtualDevice) devPath() string {
	return d.Dev.DevicePath
}

// ************** crypt device APIs *****************
func executeLuksCommand(d CryptDevice, luksCmd string) error {
	cmd := ""
<<<<<<< HEAD
	key := d.Key
	device := d.VDev.Image.devPath()
	name := d.Name
	switch(luksCmd) {
		case constLuksCmdFormat:
			cmd = fmt.Sprintf("printf %s | cryptsetup -q luksFormat %s -", key, device)
		case constLuksCmdOpen:
			cmd = fmt.Sprintf("printf %s | cryptsetup luksOpen %s %s -", key, device, name)
		case constLuksCmdClose:
			cmd = fmt.Sprintf("cryptsetup luksClose %s -", name)
			
		default:
			return errors.New(fmt.Sprintf("invalid luks command: %s", luksCmd))	
	}
	
=======
	switch luksCmd {
	case constLuksCmdFormat:
		cmd = fmt.Sprintf("printf %s | cryptsetup -q luksFormat %s -", d.Key, d.BaseImage)
	case constLuksCmdOpen:
		cmd = fmt.Sprintf("printf %s | cryptsetup luksOpen %s %s -", d.Key, d.BaseImage, d.Name)
	case constLuksCmdClose:
		cmd = fmt.Sprintf("cryptsetup luksClose %s -", d.Name)

	default:
		return errors.New(fmt.Sprintf("invalid luks command: %s", luksCmd))
	}

>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	if out, err := runCmd(cmd); err != nil {
		logrus.Errorf("failed to execute luks command %s, error: %s, out: %s", luksCmd, err.Error(), out)
		return err
	}

	return nil
}

func (d CryptDevice) Create(size int64) error {
	// compute required size for the raw image file
	sz := safeSize(size + computeFsOverhead(size) + computeCryptOverhead(size))

	// create raw image file
<<<<<<< HEAD
	return d.Image.Create(sz)
}

func (d CryptDevice) ImportData(dataPath string) error {
	if err := d.Image.Get(); err != nil {return err}
	
	defer func(){
		if err := d.Image.Put(); err != nil {return err}
	}()
	
	// apply luks format on the loop mounted image
	if err := executeLuksCommand(d, constLuksCmdFormat); err != nil { 
=======
	if err := createImageFile(d.BaseImage, sz); err != nil {
		return err
	}

	return nil
}

func (d CryptDevice) Format() error {
	// apply luks format on the raw image
	if err := d.executeLuksCommand(constLuksCmdFormat); err != nil {
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
		return err
	}

	// open luks device to create file system on the encrypted device
	if err := executeLuksCommand(d, constLuksCmdOpen); err != nil {
		return err
	}
<<<<<<< HEAD
	
	defer func(){
		if err := executeLuksCommand(d, constLuksCmdClose); err != nil {return err}	
=======

	defer func() {
		// close luks device
		_ = d.executeLuksCommand(constLuksCmdClose)
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	}()

	// setup plain device and perform file system format
<<<<<<< HEAD
	d.VDev.Dev.DevicePath := path.Join(constDevMapperPrefix, d.Name)
	if err := d.VDev.Dev.ImportData(dataPath); err != nil {return err}
	
=======
	if err := d.Device.Format(); err != nil {
		return err
	}

>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	return nil
}

func (d CryptDevice) Get() error {
<<<<<<< HEAD
	if err := d.Image.Get(); err != nil {return err}
	
	if err := executeLuksCommand(d, constLuksCmdOpen); err != nil {
		return err
	}
	
	// mount device
	d.VDev.Dev.DevicePath := path.Join(constDevMapperPrefix, d.Name)
	if err := d.VDev.Dev.Get(); err != nil {return err}
	
=======
	// open luks device
	if err := d.executeLuksCommand(constLuksCmdOpen); err != nil {
		return err
	}

	// setup plain device and perform m
	if err := d.Device.Get(); err != nil {
		return err
	}

>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	return nil
}

func (d CryptDevice) Put() error {
<<<<<<< HEAD
	// unmount the device
	if err := d.VDev.Dev.Put(); err != nil {return err}
	
=======
	// Put plain device first which is created on top of encrypted device
	if err := d.Device.Put(); err != nil {
		return err
	}

>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	// close luks device
	if err := executeLuksCommand(d, constLuksCmdClose); err != nil {
		return err
	}
<<<<<<< HEAD
	
	return d.Image.Put()
}

func (d CryptDevice) Remove() error {
=======

	return nil
}

func (d CryptDevice) Remove() error {
	// close device if already open, ignore errors if already closed
	_ = d.Put()

>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	// remove encrypted raw image file
	return d.VDev.Remove()
}

// ************** verity device APIs *****************
func getRootHash(out string) string {
	// split lines
	lines := strings.Split(out, "\n")

	if len(lines) != 9 {
		return ""
	}

	rootHashLine := strings.Split(lines[8], ":")
	if len(rootHashLine) != 2 {
		return ""
	}

	return strings.TrimSpace(rootHashLine[1])
}

func executeVerityCommand(d VerityDevice, verityCmd string) error {
	cmd := ""
<<<<<<< HEAD
	dataDev := d.VDev.Image.devPath()
	hashDev := d.HashImage
	hash := d.RootHash
	name := d.Name
	
	switch(verityCmd) {
		case constVerityCmdFormat:
			cmd = fmt.Sprintf("veritysetup format %s %s", dataDev, hashDev)
		case constVerityCmdCreate:
			cmd = fmt.Sprintf("veritysetup create %s %s %s %s", name, dataDev, hashDev, hash)
		case constVerityCmdRemove:
			cmd = fmt.Sprintf("veritysetup remove %s", name)
		case constVerityCmdVerify:
			cmd = fmt.Sprintf("veritysetup verify %s %s %s", dataDev, hashDev, hash)
		default:
			return errors.New(fmt.Sprintf("invalid luks command: %s", verityCmd))	
	}

	out := ""	
=======
	switch verityCmd {
	case constVerityCmdFormat:
		cmd = fmt.Sprintf("veritysetup format %s %s", d.BaseImage, d.HashDevice)
	case constVerityCmdCreate:
		cmd = fmt.Sprintf("veritysetup create %s %s %s %s", d.Name, d.BaseImage, d.HashDevice, d.RootHash)
	case constVerityCmdRemove:
		cmd = fmt.Sprintf("veritysetup remove %s", d.Name)
	case constVerityCmdVerify:
		cmd = fmt.Sprintf("veritysetup verify %s %s %s", d.BaseImage, d.HashDevice, d.RootHash)
	default:
		return errors.New(fmt.Sprintf("invalid luks command: %s", verityCmd))
	}

	out := ""
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	if out, err := runCmd(cmd); err != nil {
		logrus.Errorf("failed to execute verity command %s, error: %s, out: %s", verityCmd, err.Error(), out)
		return err
	}

	// parse output to read root hash and save it for future use
	if verityCmd == constVerityCmdFormat {
		d.RootHash = getRootHash(out)
		if d.RootHash == "" {
			return errors.New("Invalid root hash after verity format")
		}
	}

	return nil
}

func (d VerityDevice) Create(size int64) error {
	// compute required size for the raw image file
	sz := safeSize(size + computeFsOverhead(size))
<<<<<<< HEAD
	
	if err := d.VDev.Create(sz); err != nil {return err}
}

func (d VerityDevice) ImportData(dataPath string) error {
	// import data
	if err := d.VDev.ImportData(dataPath); err != nil {return err}
	
	// execute verity format on the data device
	return executeVerityCommand(d, constVerityCmdFormat)
=======

	// create raw image file
	if err := createImageFile(d.BaseImage, sz); err != nil {
		return err
	}
	return nil
}

func (d VerityDevice) Format() error {
	// format varity device and store root hash
	if err := d.executeVerityCommand(constVerityCmdFormat); err != nil {
		return err
	}

	return nil
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
}

func (d VerityDevice) Get() error {
	if err := d.VDev.Image.Get(); err != nil {return err}
	
	// create verity device
	if err := executeVerityCommand(d, constVerityCmdCreate); err != nil {
		return err
	}
<<<<<<< HEAD
	
	d.VDev.Dev.DevicePath := path.Join(constDevMapperPrefix, d.Name)
	if err := d.VDev.Get(); err != nil {return err}
=======

	// mount plain verity enabled device
	if err := d.Device.Get(); err != nil {
		return err
	}
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c

	return nil
}

func (d VerityDevice) Put() error {
	// put plain device first
	d.VDev.Dev.DevicePath = path.Join(constDevMapperPrefix, d.Name)
	if err := d.VDev.Put(); err != nil {
		return err
	}

	// remove verity device
	if err := executeVerityCommand(d, constVerityCmdRemove); err != nil {
		return err
	}

	return nil
}

func (d VerityDevice) Remove() error {
<<<<<<< HEAD
=======
	// Put device before removing, ignore errors from Put
	_ = d.Put()

>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	// remove raw image file
	return d.VDev.Remove()
}

// ************** crypt-verity device APIs *****************

func (d CryptVerityDevice) Create(size int64) error {
	// use encrypted deive to create raw image
	sz := safeSize(size + computeCryptOverhead(size) + computeFsOverhead(size))
<<<<<<< HEAD
	if err := d.VDev.Create(sz); err != nil {
=======
	if err := createImageFile(d.BaseImage, sz); err != nil {
		return err
	}

	// setup luks format and file system format
	d.Crypt.BaseImage = d.BaseImage
	if err := d.Crypt.Format(); err != nil {
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
		return err
	}

	return nil
}

func (d CryptVerityDevice) ImportData(dataPath string) error {
	// setup encryption for the device
	cryptDev := CryptDevice {
		VDev: d.VDev,
		Cipher: d.Cipher,
		HashType: d.HashType,
		Key: d.Key,
		KeySize: d.KeySize,
		Name: d.Name
	}
	if err := cryptDev.ImportData(dataPath); err != nil {return err}
	
	// setup integrity protection for the device
	verityDev := VerityDevice {
		VDev: d.VDev,
		HashImage: d.HashImage,
		RootHash: d.RootHash,
		Name: d.Name
	}
	if err := executeVerityCommand(verityDev, constVerityCmdFormat); err != nil {
		return err
	}
<<<<<<< HEAD
	
	// save root hash
	d.RootHash = verityDev.RootHash
	
=======

>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	return nil
}

func (d CryptVerityDevice) Get() error {
	// create verity device first
	verityDev := VerityDevice {
		VDev: d.VDev,
		HashImage: d.HashImage,
		RootHash: d.RootHash,
		Name: d.Name
	}
	if err := executeVerityCommand(verityDev, constVerityCmdCreate); err != nil {
		return err
	}
<<<<<<< HEAD
	
	cryptDev := CryptDevice {
		VDev: d.VDev,
		Cipher: d.Cipher,
		HashType: d.HashType,
		Key: d.Key,
		KeySize: d.KeySize,
		Name: d.Name
	}
	if err := executeLuksCommand(cryptDev, constLuksCmdOpen)
	
	d.VDev.Dev.DevicePath := path.Join(constDevMapperPrefix, d.Name)
	if err := d.VDev.Get(); err != nil {return err}
=======

	// open luks device on top of verity device
	d.Crypt.BaseImage = path.Join(constDevMapperPrefix, d.Verity.Name)
	if err := d.Crypt.Get(); err != nil {
		return err
	}
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c

	return nil
}

func (d CryptVerityDevice) Put() error {
	d.VDev.Dev.DevicePath := path.Join(constDevMapperPrefix, d.Name)
	if err := d.VDev.Put(); err != nil {return err}

	cryptDev := CryptDevice {
		VDev: d.VDev,
		Cipher: d.Cipher,
		HashType: d.HashType,
		Key: d.Key,
		KeySize: d.KeySize,
		Name: d.Name
	}
<<<<<<< HEAD
	if err := executeLuksCommand(cryptDev, constLuksCmdClose)
	
=======

>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	// remove verity device
	verityDev := VerityDevice {
		VDev: d.VDev,
		HashImage: d.HashImage,
		RootHash: d.RootHash,
		Name: d.Name
	}
	if err := executeVerityCommand(verityDev, constVerityCmdRemove); err != nil {
		return err
	}

	return nil
}

func (d CryptVerityDevice) Remove() error {
<<<<<<< HEAD
	return d.VDev.Remove()
}
// ***********************************************************

=======
	// Put the device if before remove, ignore errors from Put if already called
	_ = d.Put()

	return os.Remove(d.BaseImage)
}

// ***********************************************************
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
