package cryptsetup

import (
<<<<<<< HEAD
	"fmt"
=======
>>>>>>> 3bc695440f828ff410907cd2c5028cd1b6daec5c
	"log"
	"testing"
	"time"
)

func createPlainDevice() (PlainDevice, error) {
	pd := PlainDevice{
		DevicePath: "/tmp/test-plain.img",
		FsType:     "ext4",
		Mnt:        "/tmp/abc",
	}

	return pd, pd.Create(5)
}

/*
func TestPlainCreate(t *testing.T) {
	pd := PlainDevice{
		DevicePath: "/tmp/test-plain.img",
		FsType:     "ext4",
		Mnt:        "/tmp/abc",
	}

	if err := pd.Create(5); err != nil {
		fmt.Println(err)
		t.Fail()
	}
}

func TestPlainFormat(t *testing.T) {
	pd, err := createPlainDevice()
	if err != nil {
		t.Fail()
	}

	if err := pd.Format(); err != nil {
		t.Fail()
	}

	if err := pd.Remove(); err != nil {
		t.Fail()
	}
}
*/

func TestPlainMount(t *testing.T) {
	pd, err := createPlainDevice()
	if err != nil {
		log.Panicf("failed to create image file %s", pd.DevicePath)
		t.Fail()
	}

	if err := pd.Format(); err != nil {
		t.Fail()
		return
	}

	time.Sleep(time.Second * 3)

	if err := pd.Get(); err != nil {
		t.Fail()
		return
	}

	if err := pd.Put(); err != nil {
		t.Fail()
		return
	}

	if err := pd.Remove(); err != nil {
		t.Fail()
		return
	}
}
